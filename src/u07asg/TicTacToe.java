package u07asg;

import java.util.List;
import java.util.Optional;

enum Player { PlayerX, PlayerO }

public interface TicTacToe {

    void createBoard();

    List<Optional<Player>> getBoard();

    boolean checkCompleted();

    Optional<Player> checkVictory();

    boolean move(Player player, int i, int j);

    int winCount(Player current, Player winner);

    int increment(int value);

    void resetBoard(List<Optional<Player>> board);
}

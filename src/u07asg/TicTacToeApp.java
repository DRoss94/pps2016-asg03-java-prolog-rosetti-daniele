package u07asg;


import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/** A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private final TicTacToe ttt;
    private final JButton[][] board = new JButton[3][3];
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("TTT");
    private boolean finished = false;
    private Player turn = Player.PlayerX;
    private int moves = 0;

    private int xWinCount = 0;
    private int oWinCount = 0;
    private int evenCount = 0;
    private final JLabel winner = new JLabel("");
    private final JLabel xWin = new JLabel("X won: " + xWinCount);
    private final JLabel oWin = new JLabel("O won: " + oWinCount);
    private final JLabel even = new JLabel("Even: " + evenCount);

    private final JButton restart = new JButton("Play Again");

    private void changeTurn(){
        this.turn = this.turn == Player.PlayerX ? Player.PlayerO : Player.PlayerX;
    }

    public TicTacToeApp(TicTacToe ttt) throws Exception {
        this.ttt=ttt;
        initPane();
    }
    
    private void humanMove(int i, int j){
        if (ttt.move(turn,i,j)){
            board[i][j].setText(turn == Player.PlayerX ? "X" : "O");
            moves++;
            changeTurn();
            if (moves > 3){
                System.out.println("X winning count: "+ttt.winCount(turn, Player.PlayerX));
                System.out.println("O winning count: "+ttt.winCount(turn, Player.PlayerO));
            }
        }
        Optional<Player> victory = ttt.checkVictory();
        if (victory.isPresent()){
            restart.setEnabled(true);
            if(victory.get().equals(Player.PlayerO)) {
                oWinCount= ttt.increment(oWinCount);
                oWin.setText("O won: " + oWinCount);
            } else {
                xWinCount= ttt.increment(xWinCount);
                xWin.setText("X won: " + xWinCount);
            }
            winner.setText(victory.get()+" won!");
            finished=true;
            return;
        }
        if (ttt.checkCompleted()){
            restart.setEnabled(true);
            evenCount= ttt.increment(evenCount);
            even.setText("Even: " + evenCount);
            winner.setText("Even!");
            finished=true;
            return;
        }
    }

    private void initPane(){
        frame.setLayout(new BorderLayout());
        JPanel t=new JPanel(new BorderLayout());
        winner.setVerticalAlignment(SwingConstants.CENTER);
        winner.setHorizontalAlignment(SwingConstants.CENTER);
        JPanel tSouth=new JPanel(new FlowLayout());
        tSouth.add(xWin);
        tSouth.add(oWin);
        tSouth.add(even);
        t.add(BorderLayout.NORTH, winner);
        t.add(BorderLayout.SOUTH, tSouth);
        JPanel b = new JPanel(new GridLayout(3,3));
        for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                final int i2 = i;
                final int j2 = j;
                board[i][j]=new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> { if (!finished) humanMove(i2,j2); });
            }
        }
        JPanel s=new JPanel(new FlowLayout());
        s.add(exit);
        s.add(restart);
        restart.setEnabled(false);
        exit.addActionListener(e -> System.exit(0));
        restart.addActionListener(e -> resetTable());
        frame.add(BorderLayout.NORTH,t);
        frame.add(BorderLayout.CENTER,b);
        frame.add(BorderLayout.SOUTH,s);
        frame.setSize(200,230);
        frame.setVisible(true);
    }

    private void resetTable() {
        for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                board[i][j].setText("");

            }
        }
        ttt.resetBoard(ttt.getBoard());
        moves = 0;
        finished = false;
        restart.setEnabled(false);
    }

    public static void main(String[] args){
        try {
            new TicTacToeApp(new TicTacToeImpl("src/u07asg/ttt.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}
